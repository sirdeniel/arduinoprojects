#include <Arduino.h>
#include <Stepper.h>
const int stepsPerRevolution = 1500; // pasos por revolucion
Stepper mystepper(stepsPerRevolution, 8, 10, 9, 11);
                // pasos por revol, 

// modelo de stepper es: 28byj-48, es monopolar
// usando driver de stepper con uln2003APG
void setup() {
  mystepper.setSpeed(25); // velocidad de 20 rpm
  Serial.begin(9600);
}

void loop() {
  // un moviminento a favor de las manecills del reloj
  mystepper.step(90000);
}
