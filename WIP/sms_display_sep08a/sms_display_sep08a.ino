#include <Wire.h>
#include "RTClib.h"
#define display_i2c_address 0x3C // dirección hex del i2c
#define tinyrtc_i2c_address 0x68
//#include "SSD1306Ascii.h" // lib de pantalla oled
#include "SSD1306AsciiWire.h"

SSD1306AsciiWire oled; // reservar var oled
DateTime now;          // reservar var now
RTC_DS1307 RTC;        // reseraver rtc
void setup()
{
    pinMode(13,OUTPUT);
    digitalWrite(13, LOW);
    Wire.begin(); // start sda, scl protocol
    RTC.begin();  // start reloj

    now = RTC.now();

    // display
    //Wire.setClock(400000L);                           // frecuencia de actualización del bus
    oled.begin(&Adafruit128x64, display_i2c_address); // inicia con direccion
    oled.setFont(ZevvPeep8x16);                       // fuente del display
    oled.clear();
    oled.println("Sir Deniel");
    showDate(now, 5);
}
void loop()
{
    now = RTC.now();
    clearAndSetLine(2);
    showTime(now);
    delay(1000);
}

void showTime(DateTime now)
{
    oled.print(now.hour(), DEC);
    oled.print(':');
    oled.print(now.minute(), DEC);
    oled.print(':');
    oled.println(now.second(), DEC);
}

void showDate(DateTime now, byte line)
{
    oled.setRow(line);
    oled.print(now.year(), DEC);
    oled.print('/');
    oled.print(now.month(), DEC);
    oled.print('/');
    oled.print(now.day(), DEC);
    oled.println(' ');
}

void clearAndSetLine(byte line)
{
    oled.clearField(0, line, 1);
    oled.setCursor(0, line);
}

// void centered(String words)
// {
//     char[c] = words
// }