#define LED_BUILTIN 2

// Instructable link or attached image on cwd
// https://www.instructables.com/id/NodeMCU-ESP8266-Details-and-Pinout/
void setup()
{
    Serial.begin(115200);
    pinMode(LED_BUILTIN, OUTPUT); //Declare Pin mode
}

void loop()
{
    //inverted logic works on LED_BUILTIN
    digitalWrite(LED_BUILTIN, HIGH); //3.3V on D4 but LED is off
    Serial.printf("Pin %d: ", LED_BUILTIN);
    Serial.println("Turn off for 1 second");
    delay(1000);                    //Wait 1 second
    digitalWrite(LED_BUILTIN, LOW); //0V on D4 but LED is ON :/
    Serial.print("Turn on for 6 seconds\n");
    delay(6000); //Wait 1 second
}