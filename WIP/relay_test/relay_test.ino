#define LED_BUILTIN 2

void setup()
{
    Serial.begin(115200);
    pinMode(5, OUTPUT);
    pinMode(4, INPUT_PULLUP); //button wo resistor
    //turn of builtin led -.-
    pinMode(LED_BUILTIN, OUTPUT);
    digitalWrite(LED_BUILTIN, HIGH);
}

// the loop function runs over and over again forever
void loop()
{
    if (digitalRead(4))
    {
        Serial.println("Disconnected wires, turn off that relay for 1 second");
        digitalWrite(5, HIGH);
        delay(3000);
    }
    else
    {
        Serial.println("Connected wires, turn on that relay for 15 seconds");
        digitalWrite(5, LOW);
        delay(15000);
    }
}