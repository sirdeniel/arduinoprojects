# PIN Layout
Vista superior
![ESP32 CAM superior view](ESP32-CAM-pinout-new.png)

Vista inferior
![ESP32 CAM inferior view](ESP32-CAM-Module-PinOut.jpg)

# Programar con Arduino
Fuente: https://technoreview85.com/how-to-program-esp-32-cam-using-arduino-uno-board/

1. Arduino 5V,GND to ESP32 CAM 5V, GND.
1. Arduino RX,TX to ESP32 CAM board RX,TX
1. Solo en Arduino, RESET pin a GND
1. Solo en ESP 32 CAM, D0 a GND **Nota: esto es para programar. Desconectarlo una vez programado**

## Pasos en Arduino IDE
Una vez descargado esp32 del "Boards Manager"
1. Seleccionar ESP32 Wrover Module
1. Configurarlo de esta forma
![config](Config.jpg)
1. Usar **Programador AVR ISP**
1. Dar clic en "Upload". **Crucial:** Mientras esté en "Verifying board" (poco después de que presiones "Upload"), pulsar el botón RST del ESP32 CAM para permitir que suba.
1. Ya puedes desenergizar/desconectar el Arduino y quitar la conexión GND,D0 del ESP32. Luego vuelve a conectar y abre el Serial Monitor para ver sus prints.