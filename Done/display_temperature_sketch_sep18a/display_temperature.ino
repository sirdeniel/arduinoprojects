#include <Wire.h>
#define DSPL_ADDRESS 0x3C // dirección hex del i2c
#include <SimpleDHT.h>
#include "SSD1306Ascii.h"
#include "SSD1306AsciiWire.h"

SSD1306AsciiWire oled;

// for DHT11_1,
//      VCC: 5V or 3V
//      GND: GND
//      DATA: 2
int pinDHT11_1 = 2;

SimpleDHT11 dht1(pinDHT11_1); // iniciar variable dht11

void setup()
{
    pinMode(13, OUTPUT);
    digitalWrite(13, LOW);
    Wire.begin();
    oled.begin(&Adafruit128x64, DSPL_ADDRESS); // iniciar display
    oled.setFont(ZevvPeep8x16); // fuente con caracteres espaciados como matriz
    oled.clear(); // limpiar pantalla, siempre realizarlo
    oled.set2X(); // fuente dos veces grande
    clearAndSetLine(2);
}

void loop()
{
    // start working...

    // read without samples.
    byte temperature = 0;
    byte humidity = 0;
    int err = SimpleDHTErrSuccess;
    if ((err = dht1.read(&temperature, &humidity, NULL)) != SimpleDHTErrSuccess)
    {
        oled.print("error1= ");
        oled.print(err);
        delay(1000);
        clearAndSetLine(2);
    }

    oled.print(" Sensor 1");
    oled.print((int)temperature);
    oled.print(" *C, ");
    oled.print((int)humidity);
    oled.print(" H");
    delay(1000);

    // DHT11 sampling rate is 1HZ.
    //delay(1500);
    clearAndSetLine(2);
}

void clearAndSetLine(byte line)
{
    oled.clearField(0, line, 1);
    oled.setCursor(0, line);
}
