/****************************************
 * Include Libraries
 ****************************************/
#include "UbidotsESPMQTT.h"

/****************************************
 * Define Constants
 ****************************************/
#define TOKEN "BBFF-lYrttrlPaoPSTs5jAt4pxM9VeVa2VB" // Your Ubidots TOKEN
#define WIFINAME "LANA&PONY"                       //Your SSID
#define WIFIPASS "pachis805"                        // Your Wifi Pass

uint16_t count = 0;
unsigned long sendDataPrevMillis = 0;

Ubidots client(TOKEN);

/****************************************
 * Auxiliar Functions
 ****************************************/

void callback(char *topic, byte *payload, unsigned int length)
{
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++)
  {
    Serial.print((char)payload[i]);
  }
  Serial.println();
}

/****************************************
 * Main Functions
 ****************************************/

void setup()
{
  // put your setup code here, to run once:
  Serial.begin(115200);
  client.ubidotsSetBroker("industrial.api.ubidots.com"); // Sets the broker properly for the industrial account
  client.setDebug(true);                                 // Pass a true or false bool value to activate debug messages
  client.wifiConnection(WIFINAME, WIFIPASS);
  client.begin(callback);
  //to subscribe, device_label and variable_label are needed to know, maybe it should exist
  client.ubidotsSubscribe("my-new-device", "temperature"); //Insert the dataSource and Variable's Labels
}

void loop()
{
  // put your main code here, to run repeatedly:
  if (!client.connected())
  {
    client.reconnect();
    //client should resubscribe to topic every reconnection
    client.ubidotsSubscribe("my-new-device", "temperature"); //Insert the dataSource and Variable's Labels
  }

  // Publish values to 2 different data sources
  if (millis() - sendDataPrevMillis > 15000)
  {
    sendDataPrevMillis = millis();
    count++;
    client.add("temperature", count*1.5);
    client.ubidotsPublish("my-new-device"); //device name can be an existing one, otherwise it creates
    //also, it reconnects every 3 publishes
    //maybe its every 45 seconds or every 3 publishes
  }

  //client.loop should not be interrupted by any means, e.g. by a delay function
  client.loop();

}
