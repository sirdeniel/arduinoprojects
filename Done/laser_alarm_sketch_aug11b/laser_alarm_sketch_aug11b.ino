/*
How to use a laser to trigger an alarm
and thus with button to make "alarm off" 
with a push of a button.
*/
#define DETECT 2  // pin 2 for  sensor
#define ACTION 13 // pin 13 for action to do someting 13 is led in arduino
#define BUTTON 7

void setup()
{
  Serial.begin(9600);
  // Template is here:
  Serial.println("Template: Robojax.com Laser Module Test");
  pinMode(DETECT, INPUT);        //define detect input pin
  pinMode(ACTION, OUTPUT);       //define ACTION output pin
  pinMode(BUTTON, INPUT_PULLUP); // no need for resistor, great!
  digitalWrite(ACTION, LOW);
  // Laser sensor code for Robojax.com
}

void loop()
{
  // Template from this source:
  // Laser Sensor code for Robojax.com

  int detected = digitalRead(DETECT); // read Laser sensor

  if (detected == HIGH)
  {
    // detected HIGH means laser is pointing to detector
    // thus there is no obstacule
    digitalWrite(ACTION, LOW); // all good, no alarm
    Serial.println("Detected!");
  }
  else
  {
    digitalWrite(ACTION, HIGH); // Set the buzzer OFF
    Serial.println("No laser!");

    while (!digitalRead(BUTTON))
    { // while button is not pushed yet
      // alarm!!, LED is already ON, so this loop is just a "trap"
    }
  }
  delay(200);
}
