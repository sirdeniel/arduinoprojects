
[TOC]

# Requiremnts
The Arduino IDE version required is: ```1.8.9```
# Folder structure
- **Done** Code reviewed, commented and with pictures instructing how to connect wires.
- **FritzingPartsDownloaded** Folder with .fzpz models
- **PartialComplete** Means code is useful but comments and pictures has to be done. Also, special steps for setting up the project needs to be explained easily.
- **WIP** States "Work In Progress", code is not useful or optimized yet.

# Hacks/Useful stuff
## VSCode settings
Use VSCode 1.42.1 as newer version has bug on serial monitor. https://code.visualstudio.com/updates/v1_42

## Libraries
- **AnalogMultiButton** Capture button presses (by event: hold, released, pressed by a duration, etc) on a single analog pin
- **VirtualDelay** Non blocking delay (like ```millis()``` but more easily implemented)
## ESP8266 Pin Layout

Source: https://tttapa.github.io/ESP8266/Chap04%20-%20Microcontroller.html


| GPIO | Function             | State            | Restrictions                                                                      |
|------|----------------------|------------------|-----------------------------------------------------------------------------------|
| 0    | Boot mode select     | -                | No Hi-Z                                                                           |
| 1    | TX0                  | -                | Not usable during Serial transmission                                             |
| 2    | Boot mode select TX1 | 3.3V (boot only) | Don’t connect to ground at boot time Sends debug data at boot time                |
| 3    | RX0                  | -                | Not usable during Serial transmission                                             |
| 4    | SDA (I²C)            | -                | -                                                                                 |
| 5    | SCL (I²C)            | -                | -                                                                                 |
| 6-11 | Flash connection     | x                | Not usable, and not broken out                                                    |
| 12   | MISO (SPI)           | -                | -                                                                                 |
| 13   | MOSI (SPI)           | -                | -                                                                                 |
| 14   | SCK (SPI)            | -                | -                                                                                 |
| 15   | SS (SPI)             | 0V               | Pull-up resistor not usable                                                       |
| 16   | Wake up from sleep   | -                | No pull-up resistor, but pull-down instead  Should be connected to RST to wake up |

