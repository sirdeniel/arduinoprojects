#include <Wire.h>
#include <RtcDS3231.h>
#define GEN_ON 2
#define PREVENT_BREAKER 3

RtcDS3231<TwoWire> Rtc(Wire);

void setup()
{
    Serial.begin(9600);
    // led 13 is always off, it's an alarm for next checks
    pinMode(13, OUTPUT);
    digitalWrite(13, LOW);

    // SET RELAY CONTROL PINS
    pinMode(GEN_ON, OUTPUT);
    pinMode(PREVENT_BREAKER, OUTPUT);
    digitalWrite(GEN_ON, HIGH);
    digitalWrite(PREVENT_BREAKER, HIGH);

    // SETUP RTC RtcDS3231 and some checks and a warning if needed
    Rtc.Begin();
    RtcDateTime compiled = RtcDateTime(__DATE__, __TIME__);
    if (!Rtc.IsDateTimeValid())
    {
        Rtc.SetDateTime(compiled);
        digitalWrite(13, HIGH);
    }
    if (!Rtc.GetIsRunning())
    {
        Rtc.SetIsRunning(true);
    }

    // never assume the Rtc was last configured by you, so
    // just clear them to your needed state
    Rtc.Enable32kHzPin(false);
    Rtc.SetSquareWavePin(DS3231SquareWavePin_ModeNone);
}

void loop()
{
    if (!Rtc.IsDateTimeValid())
    {
        if (Rtc.LastError() != 0)
        {
            // oops :c
            digitalWrite(13, HIGH);
        }
    }
    RtcDateTime now = Rtc.GetDateTime();
    if (there_is_alarm(now))
    {
        generatorOn(5);
    }
    printDateTime(now);
    delay(1000); // mmm, hope this ain't bring problems
}

bool there_is_alarm(const RtcDateTime &dt)
{
    // morning alarm
    if (dt.Minute() == 25)
    {
        return true;
    }
    
    if (dt.Minute() == 55)
    {
        return true;
    }


    return false;
}

void generatorOn(int minutes)
{
    // start generator
    digitalWrite(PREVENT_BREAKER, LOW);
    delay(500);
    digitalWrite(GEN_ON, LOW);

    // minutes of generator on (stablished by user)
    delay(minutes * 60 * 1000);

    // stop generator
    digitalWrite(GEN_ON, HIGH);
    delay(10000);
    digitalWrite(PREVENT_BREAKER, HIGH);
}


#define countof(a) (sizeof(a) / sizeof(a[0]))

void printDateTime(const RtcDateTime& dt)
{
    char datestring[20];

    snprintf_P(datestring, 
            countof(datestring),
            PSTR("%02u/%02u/%04u %02u:%02u:%02u"),
            dt.Month(),
            dt.Day(),
            dt.Year(),
            dt.Hour(),
            dt.Minute(),
            dt.Second() );
    Serial.println(datestring);
}
