#include <Servo.h>
Servo pinza;
Servo base;
Servo homiz;
Servo homde;
char letra = 0;
int da = 3;
int ia = 2;
int dd = 5;
int id = 6;
void setup()
{
  Serial.begin(9600);
  base.attach(A0);
  base.write(90);
  homde.attach(A1);
  homde.write(90);
  homiz.attach(A2);
  homiz.write(90);
  pinza.attach(A3);
  pinza.write(90);
  pinMode(ia, OUTPUT);
  pinMode(da, OUTPUT);
  pinMode(id, OUTPUT);
  pinMode(dd, OUTPUT);
  digitalWrite(ia, HIGH);
  digitalWrite(da, HIGH);
  digitalWrite(id, HIGH);
  digitalWrite(dd, HIGH);
}

void loop()
{
  if (Serial.available() > 0)

    letra = Serial.read();
  if (letra == 'i') //////////////////////(i,k,l,j,u)= Control del brazo///////////////
  {
    turn_up();
  }
  if (letra == 'k')
  {
    turn_down();
  }
  if (letra == 'l')
  {
    arm_right();
  }
  if (letra == 'j')
  {
    arm_left();
  }
  if (letra == 'u')
  {
    cerrar_pinza();
  }
  if (letra == 'w') /////////////////////(w,s,a,d)=Control de las ruedas///////////////
  {
    adelante();
  }
  if (letra == 'r')
  {
    reset();
  }
  if (letra == 'y')
  {
    abrir_pinza();
  }
  if (letra == 'd')
  {
    car_right();
  }
  if (letra == 'a')
  {
    car_left();
  }
  if (letra == 's')
  {
    atras();
  }
} ///////////////////////////////////////////////////////Funciones///////////////////////////////////////////////////////////////////
int turn_up()
{
  homiz.write(140);
  homde.write(50);
}
int reset()
{
  homiz.write(90);
  homde.write(90);
  base.write(90);
  pinza.write(90);
}
int arm_right()
{
  base.write(0);
}
int arm_left()
{
  base.write(180);
}
int cerrar_pinza()
{
  pinza.write(50);
}
int abrir_pinza()
{
  pinza.write(90);
}
int turn_down()
{
  homde.write(90);
  homiz.write(90);
}
int adelante()
{
  digitalWrite(dd, LOW);
  digitalWrite(id, LOW);
}
int atras()
{
  digitalWrite(da, LOW);
  digitalWrite(ia, LOW);
}
int car_right()
{
  digitalWrite(ia, LOW);
  digitalWrite(da, LOW);
}
int car_left()
{
  digitalWrite(dd, LOW);
  digitalWrite(ia, LOW);
}
