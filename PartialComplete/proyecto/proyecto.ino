#include <Stepper.h> // lib para controlar stepper
#include <Wire.h> // lib para habilitar modulos i2c
#include <JC_Button.h> // lib para simplificar debouncing

//variables de pantalla
#define I2C_ADDRESS 0x3C // dirección hex del i2c
#include "SSD1306Ascii.h" // lib de pantalla oled
#include "SSD1306AsciiWire.h" // lib extensión de pantalla oled
SSD1306AsciiWire oled; // inicializar variable

// inicializar variables de botones, la sintaxis es:
// Button(pin, tiempo_debounce, pull_up?, invertido?);
// Button(int,      int       , boolean ,  boolean  )
// Si solo es Button(n); entonces significa por default:
// conectado al pin n, 25ms debounce, pullup sí, pulsado es HIGH
Button UP(7); Button DOWN(6); Button START(5);

// variables de solicitudes
unsigned int cant = 0; // la cantidad de masa solicitada
boolean switchValor = false; // false "gramos", true "calorías"

// modelo de stepper es: 28byj-48, es unipolar
// usando driver de stepper con uln2003APG
const int stepsPerRevolution = 1500; // pasos por revolucion
Stepper mystepper(stepsPerRevolution, 8, 10, 9, 11);
             // pasos por revolución, pines[..]

void setup() {
  // configurar i2c con protocolo sda, scl
  Wire.begin(); // iniciar sda, scl
  Wire.setClock(400000L); // frecuencia de actualización/trabajo


  //motor
  mystepper.setSpeed(26); // velocidad tope sin errores: 26 rpm

  //botones
  UP.begin(); DOWN.begin(); START.begin(); // iniciar

  //pantalla
  //  .begin( id_dispositivo, dirección hex)
  oled.begin(&Adafruit128x64, I2C_ADDRESS); // iniciar con
  oled.setFont(Callibri11); // tipo de fuente para el texto
  oled.clear(); // limpia la pantalla, pues puede tener en buffer
  
  //_titulo de pantalla
  oled.setCursor(     22,    0); // ubicacion del cursor en pantalla
  //  .setCursor(columna, fila); col entre 0 y 127; fila entre 0 y 7
  
  oled.println("MASA CONTROLADA"); // texto con salto de línea
  oled.println("        ------------------------    "); // + texto
  oled.setCursor(28, 4); // reubicacion del cursor
  oled.println("Listo para gramos"); // texto con salto de línea
}

void loop() {
  // leer botones (esta con debounce)
  UP.read(); DOWN.read(); START.read();
  
  // .wasPressed solo recibe un pulso del boton como señal
  // si lo mantiene pulsado, no hay mas señales hasta soltar
  // y volver a pulsar

  // si pulsa el botón Subir
  if (UP.wasPressed()) {
    cant += 1; // aumentar cantidad
    clearLine(false); // borrar línea para mostrar cantidad
    
    oled.print(cant*30, DEC); // mostrar 30 veces cantidad
	printTag(); // mostrar tag de solicitud
  }

  // si pulsa el botón Bajar
  if (DOWN.wasPressed()) {
    if(cant>0){ // si es mayor a cero
      cant -= 1; // restar cantidad
    }
    clearLine(false); // borrar línea para mostrar cantidad
    oled.print(cant*30, DEC); // mostrar 30 veces cantidad
    printTag(); // mostrar tag de solicitud     
  }

  // si pulsa Iniciar y no hay solicitud (0 gr ó 0 cal)
  if (START.wasPressed() && cant==0) {
    // entonces cambiar entre gramos y calorías
    switchValor = !switchValor; // cambiar logica
    clearLine(true); // borrar línea para mostrar texto ancho
    oled.print("Listo para"); // texto sin salto de línea
	printTag(); // mostrar tag de solicitud
  }

  // si presiono una vez iniciar y espera una cantidad
  if (START.wasPressed() && cant >0) {
    clearLine(true); // borrar línea para mostrar texto ancho
	
	// en una sola línea, mostrar:
    oled.print("Dispensando "); // texto (sin salto de línea)
    oled.print(cant*30); // mostrar 30 veces cantidad
	printTag(); // mostrar tag de solicitud
	
    mystepper.step(cant*2075); // girar cant vueltas tornillo
    cant = 0; // reset cantidad
    clearLine(true); // borrar línea para mostrar texto ancho
    oled.print("Listo para"); // texto sin salto de línea
	printTag(); // mostrar tag de solicitud
  }

  
}

/**
 * Limpiar un par de líneas de la pantalla
 * sin afectar el resto de la pantalla, es decir, 
 * solo actualiza una parte de la pantalla
 **/
void clearLine(boolean esTextoAncho) {
  oled.clearField(0, 3, 128); // limpiar pantalla 
  oled.clearField(0, 4, 128); // limpiar pantalla
  //  .clearFiled(col, fila, n_caracteres)
  //  Significa: limpiar desde col, fila
  //  n caractares ingresados en la línea
  
  if (esTextoAncho) { // si es texto ancho
    oled.setCursor(28, 4); // ubicar cursor en columna 28
  }
  else { // de otro modo
    oled.setCursor(40, 4); // ubicar cursor en columna 40
  }
}
/**
 * Muestra en text el tag correspondiente al tipo de
 * solicitud del usuario, es decir,
 * mostrar entre calorías y gramos
 **/
void printTag() {
  if (switchValor) { // si es preferencia por calorías
    oled.print(" calorías"); // texto sin salto de línea
  }
  else {
    oled.print(" gramos"); // texto sin salto de línea
  }
}


